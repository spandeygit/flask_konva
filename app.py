import json
import flask
import numpy as np
from flask import request


app = flask.Flask(__name__)

state = ""

@app.route("/")
def index():
    mydata = [200,300,500,700]
    return flask.render_template("index.html", dataset = mydata)


@app.route("/myapp")
def index_myapp():
    mydata = [200,300,500,700]
    return flask.render_template("myapp.html", dataset = mydata)
    


@app.route("/trial")
def index_try():
    mydata = [200,300,500,700]
    return flask.render_template("trial.html", dataset = mydata)
    



    
@app.route("/drag")
def index_drag():
    mydata = [200,300,500,700]
    return flask.render_template("drag_group.html", dataset = mydata)
    
    

@app.route("/connect")
def index_connect():
    mydata = [200,300,500,700]
    return flask.render_template("connect.html", dataset = mydata)

@app.route("/tween")
def index_tween():
    mydata = [200,300,500,700]
    return flask.render_template("tween.html", dataset = mydata)


@app.route('/restoregraph',methods = ['POST', 'GET'])
def restoreg():
       global state
       if request.method == 'POST':
           pass;
       else:
           print(state); 
           return json.dumps({"result": state})


@app.route('/savegraph',methods = ['POST', 'GET'])
def saveg():
       global state
       if request.method == 'POST':
           pass;
       else:
            d = request.args.get('dt')
            state = d;
            print("GOT JSON DATA", d);
            d = json.loads(d)
            #for x in d.keys():
            #    print(x);
            return json.dumps({"result": d})



@app.route("/data")
@app.route("/data/<int:ndata>")
def data(ndata=100):
    """
    On request, this returns a list of ``ndata`` randomly made data points.

    :param ndata: (optional)
        The number of data points to return.

    :returns data:
        A JSON string of ``ndata`` data points.

    """
    x = 10 * np.random.rand(ndata) - 5
    y = 0.5 * x + 0.5 * np.random.randn(ndata)
    A = 10. ** np.random.rand(ndata)
    c = np.random.rand(ndata)
    return json.dumps([{"_id": i, "x": x[i], "y": y[i], "area": A[i],
        "color": c[i]}
        for i in range(ndata)])



if __name__ == "__main__":
    import os
    #os.system("open http://localhost:{0}".format(port))
    app.debug = True
    app.run(host='0.0.0.0', port=8000)

